# Lebenslauf

## Kontakdaten

Name: Serghei
Mail: no-reply@sergheis.com
Adresse: 78120 Furtwangen Robert-Gerwig-Platz 1
Telefon: 07723 920 0

## Bildung

- 2020-2024
    _Hochschule Furtwangen_
    Online Medien - Bachelor of Science

## Karriere

- 2023-Heute
    _Beispiel GmbH 2_
    Werkstudent in der Softwareentwicklung
- 2022-2023 
    _Beispiel GmbH 1_
    Werkstudent in der Softwareentwicklung
- 2022
    _Beispiel GmbH 1_
    Praktikant in der Softwareentwicklung

## Skills

### Programmiersprachen
- JavaScript/TypeScript/HTML/CSS (gut)
    - Svelte (gut)
    - Angular (gut)
    - Astro (gut)
    - Vue (fortgeschritten)
    - React (Grundkenntnisse)
    - NextJS (Grundkenntnisse)
- Go (fortgeschritten)
- PHP (fortgeschritten)
- Java (Anfänger)

### Softskills
- Teamfähigkeit
- Anpassungsfähigkeit
- Hohe Lernbereitschaft

## Sprachen
- Englisch
- Deutsch
- Russisch
